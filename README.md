# Language Translation App

Development Practice Challenge : Build a Language Translation App
from Topcoder

Check our Gitlab here : https://gitlab.com/ulilazmi3/language-translation-app

Check the deployment here : https://languagetranslationapp.netlify.app/

Has been tested with "Indonesian" with code "id" as source language and "Arabic" with code "ar" as target language, it also has trasliteration.

## How to Deploy

1. Open the folder using 'terminal' such as in VSCode.

2. Run "npm install" to install dependencies.

3. Run "npm run build" to create 'dist' folder for deployment.

4. Deploy programs using dist, for example in netlify you can drag it to the tray.

Or you can also use Dist folder if it's provided.

## How to Run

Before everything you should pass CORS Policy problem because the API, you can use chrome extension such as "Moesif Orign & CORS Changer", "Allow CORS: Access-Control-Allow-Origin", or check here https://stackoverflow.com/questions/46522749/how-to-solve-redirect-has-been-blocked-by-cors-policy-no-access-control-allow

1. Open the program.

2. Select the source language (there's also detect language which auto detect).

3. Write things that you want to be translated.

4. Select target language.

5. Click translate.

6. You can see the translation below, if the target language is using other type of alphabet there would be the transliteration too.